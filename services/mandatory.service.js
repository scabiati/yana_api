/*
Definition
*/
    const Mandatories = {
       bookmark: ['type', 'bookmark'],
       identity: ['email', 'password'],
       register: ['email', 'password', 'repeatepassword', 'firstname', 'lastname', 'birthdate']
    };
//

/*
Export
*/
    module.exports = Mandatories;
//
