/*
Import
*/
    const mongoose = require('mongoose');
    const { Schema } = mongoose;
//

/*
Definition
*/
    const MySchema = new Schema({
        user_id: String,
        type: String,
        bookmark: String
    });
//

/*
Set index for search
*/
    MySchema.index({
        type: 'text',
        bookmark: 'text'
    })
//


/*
Export
*/
    module.exports = mongoose.model( 'post', MySchema );
//
