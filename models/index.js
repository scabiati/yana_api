/*
Definition
*/
    const Models = {
        identity: require('./identity.model'),
        user: require('./user.model'),
        bookmark: require('./bookmark.model')
    };
//

/*
Export
*/
    module.exports = Models;
//
