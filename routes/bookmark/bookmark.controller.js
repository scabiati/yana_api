// Imports
const Models = require('../../models/index')

/*
Methods CRUD
*/
    const createItem = (req) => {
        return new Promise( (resolve, reject) => {
            Models.bookmark.create({
                ...req.body,
                user_id: req.user._id
            })
            .then( bookmark => resolve({bookmark, identity: req.user}) )
            .catch( err => reject(err) );
        })
    }

    const readItem = (req) => {
        return new Promise( (resolve, reject) => {
            Models.bookmark.find({"user_id": req.user._id}, (err, collection) => {
                err ? reject(err) : resolve(collection);
            })
        })
    }

    const deleteItem = (req) => {
        return new Promise( (resolve, reject) => {
            Models.bookmark.deleteOne({ _id: req.params.id, user_id: req.user._id }, (err, document) => {
                err ? reject(err) : resolve(document);
            })
        })
    }
//

/*
Export
*/
    module.exports = {
        createItem,
        readItem,
        deleteItem
    }
//
