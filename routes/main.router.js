/*
Imports
*/
    // NodeJS
    const { Router } = require('express');
    const passport = require('passport');

    // Routers
    const BookMarkClass = require('./bookmark/bookmark.router');
    const AuthRouterClass = require('./auth/auth.router');
    const NewsRouterClass = require('./news/news.router');

    // Authentication
    const { setAuthentication } = require('../services/auth.service');
    setAuthentication(passport);
//

/*
Define routers
*/
    // Parent
    const mainRouter = Router();
    const apiRouter = Router();
    mainRouter.use('/api', apiRouter);

    // Child
    const newsRouter = new NewsRouterClass();
    const bookmarkRouter = new BookMarkClass( { passport } );
    const authRouter = new AuthRouterClass( { passport } );
//

/*
Configure routes
*/
    // Set API router
    apiRouter.use('/bookmark', bookmarkRouter.init());
    apiRouter.use('/auth', authRouter.init());
    apiRouter.use('/news', newsRouter.init());

//

/*
Export
*/
    module.exports = { mainRouter };
//
