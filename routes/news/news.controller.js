const axios = require('axios');

const getSources = () => {
    const request = axios.get(`${process.env.NEWS_API_URL}/sources?apikey=${process.env.NEWS_API_TOKEN}`)
    return request
}

const getNews = () => {
    const request = axios.get(`${process.env.NEWS_API_URL}/top-headlines?country=fr&apikey=${process.env.NEWS_API_TOKEN}`)
    return request
}

const getNewsBySource = source => {
    const request = axios.get(`${process.env.NEWS_API_URL}/top-headlines?sources=${source}&apikey=${process.env.NEWS_API_TOKEN}`)
    return request
}

const getNewsByKeyword = keyword => {
    const request = axios.get(`${process.env.NEWS_API_URL}/top-headlines?q=${keyword}&apikey=${process.env.NEWS_API_TOKEN}`)
    return request
}

module.exports = {
    getSources,
    getNews,
    getNewsBySource,
    getNewsByKeyword
}
