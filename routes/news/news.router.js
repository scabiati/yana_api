/*
Imports
*/
// Nodes
const express = require('express');
const myRouter = express.Router();

// Modules
const { getSources, getNews, getNewsBySource, getNewsByKeyword } = require('./news.controller');
//

/*
Routes definition
*/

class MyRouterClass {

    routes(){
        // CRUD: create
        myRouter.get('/sources', (req, res) => {
            getSources()
                .then( result => {
                        return res.status(200).json({
                            message: "Sources found",
                            err: null,
                            data: result.data
                        })
                    }
                )
                .catch( err => res.status(500).json({
                    message: "unable to fetch sources",
                    err: err
                }))
        })

        myRouter.get('/:source', (req, res) => {
            getNewsBySource(req.params.source)
                .then( result => {
                    return res.status(200).json({
                        message: "News found",
                        err: null,
                        data: result.data
                    })
                })
                .catch( err => res.status(500).json({
                    message: "unable to fetch news",
                    err: err
                }))
        })

        myRouter.get('/search/:keyword', (req, res) => {
            getNewsByKeyword(req.params.keyword)
                .then( result => {
                    return res.status(200).json({
                        message: "News found",
                        err: null,
                        data: result.data
                    })
                })
                .catch( err => res.status(500).json({
                    message: "unable to fetch news",
                    err: err
                }))
        })

        myRouter.get('/', (req, res) => {
            getNews()
                .then( result => {
                    return res.status(200).json({
                        message: "News found",
                        err: null,
                        data: result.data
                    })
                })
                .catch( err => res.status(500).json({
                    message: "unable to fetch news",
                    err: err
                }))
        })
    }

    init(){
        // Get route fonctions
        this.routes();

        // Sendback router
        return myRouter;
    }
}
//

/*
Export
*/
module.exports = MyRouterClass;
//
